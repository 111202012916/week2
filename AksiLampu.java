package week2;

public class AksiLampu {

	public static void main(String[] args) {
		Lampu lampu = new Lampu();
		
		lampu.nyalakanlampu();
		System.out.println("Apakah Lampu menyala? "+lampu.nyala);
		
		lampu.matikanlampu();
		System.out.println("Apakah Lampu menyala? "+lampu.nyala);
	}

}
